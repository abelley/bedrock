[![Boite Beet](https://devbeet.com/logo-readme.png)](https://boitebeet.com/)

Base **Bedrock** de **Beet**.

Documentation complète Bedrock sur [https://roots.io/bedrock/docs/](https://roots.io/bedrock/docs/).

#### Exigences

* PHP >= 7.1
* [Composer](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx)
* [wp-cli](https://make.wordpress.org/cli/handbook/installing/) et [wp-cli-dotenv](https://github.com/aaemnnosttv/wp-cli-dotenv-command)
* [Node.js](https://nodejs.org/) _(voir note)_
* [npm](https://www.npmjs.com/) _(voir note)_
* [Gulp](https://gulpjs.com/)

_*[NVM](https://github.com/nvm-sh/nvm) est fortement recommendé pour installer Node. Il installe automatiquement NPM et permet la gestion des versions de Node._

#### Plugins Wordpress requis au thème (doivent être actifs en tout temps) :
* Advanced Custom Fields PRO
* The SEO Framework

#### Commandes disponibles

La base Bedrock utilise npm pour standardiser les différentes commandes. Elles ont toujours le même format : `npm run <action>`. Pour plus de détails, voir plus bas ou dans le `package.json`.

# Sommaire

* Création d'un nouveau projet
* Travailler sur un projet déjà existant
* Utilisation de la base d'assets
* Déploiements

---

## Création d'un nouveau projet

1. Créer un nouveau repository dans le projet du client sur Bitbucket. Ajoutez un nouveau projet s'il n'existe pas déjà.  
_\* Important de mettre boitebeet en tant que owner._  
_\** S'assurer que le repo et le projet sont private._

1. Sur votre poste, créer un nouveau dossier pour votre projet. Ensuite cloner la base Bedrock dans celui-ci :

    `git clone git@bitbucket.org:boitebeet/bedrock.git .`

1. Dans le fichier `composer.json`, supprimer ou ajouter les plugins nécessaires au projet.

1. Pour cette étape, assurez-vous d'avoir en votre possession votre url de développement et une base de données vierge. Ensuite exécutez la commande `npm run setup` et vous serez appelé à entrer les informations de votre nouveau projet.

1. Votre nouveau projet est maintenant prêt! Pour savoir comment compiler les assets, voir la section **Utilisation de la base d'assets**

---

## Travailler sur un projet déjà existant

1. Créer un nouveau dossier pour votre projet. Ensuite cloner le repo dans celui-ci :

    `git clone git@bitbucket.org:boitebeet/<repo-du-projet>.git .`

1. Importer une base de données existante et remplacer le nom de domaine pour le vôtre en local.  

    Avec wp-cli : `wp search-replace --url=<current-domain> <current-domain> <new-domain>`

1. Pour cette étape, assurez-vous d'avoir en votre possession votre url de développement. Ensuite exécutez la commande `npm run setup` et vous serez appelé à entrer les informations du projet.

1. Votre projet est maintenant prêt!

---

## Utilisation de la base d'assets

### Pour compiler les assets

1. Faire `npm install` pour installer Gulp et ses dépendances
1. Pour démarrer Gulp, faire `npm run watch`

Pour créer les versions minifiés du css et du js, faire `npm run build`. La production et le staging utilisent les versions minifiés, donc très important de le faire avant un déploiement.

### Sprite d'icônes

1. Mettre vos SVGs dans le dossier du thème /assets/images/icons
1. Ensuite faire la commande `npm run sprites` pour regénérer les sprites SVG.

Une [mixin Sass](assets/styles/base/abstracts/_icons.scss) et une [fonction PHP](includes/miscellaneous.php) existent pour utiliser les icônes par la suite. Il y a certaines limitations avec ces deux méthodes, donc travailler avec un SVG classique pour des besoins complexes.

---

## Déploiements

### Sur le staging (devbeet.com) :

S'assurer que vous pouvez vous connecter sur devbeet.com via SSH. Sinon, ajouter votre clé SSH publique sur le serveur de staging. Faire un `ssh-add`.

1. Se connecter sur le cpanel de devbeet.com et créer un nouveau sous-domaine. Le **document root** doit pointer sur `/current/web` à l'intérieur du sous-domaine.  
Exemple : `/public_html/bedrock/current/web`

1. Base de données :
    1. Créer une nouvelle base de données. _Ne pas oublier de mettre les accès dans le document!_
    1. Aller ensuite y importer la vôtre avec le nom de domaine remplacé pour le staging.  

        Avec wp-cli : `wp search-replace --url=<current-domain> <current-domain> <new-domain>`  

1. Aller modifier les infos de serveur dans `config/hosts.php`.

1. Exécuter la commande de déploiement :

    `npm run deploy staging`   

1. Sur le serveur, aller modifier dans le dossier `/shared` le fichier `.env` avec les informations relatives à l'environnement de staging.

1. Encore sur le serveur, aller porter les uploads dans le dossier `/shared/uploads`

### Sur la production :

Les étapes sont pratiquement les mêmes que pour un déploiment sur le staging. En premier lieu, s'assurer que le serveur accepte les commandes SSH et que vous ayez accès au dossier `/root`.

1. Allez ajouter votre clé SSH publique sur le serveur de production et connectez-vous ensuite en SSH. Faire un `ssh-add`.

1. Faire la connection vers Bitbucket :
    1. Générer des clés SSH sur le serveur avec `ssh-keygen`. Ne rien écrire à toutes les étapes (faire simplement [enter] partout).
    1. Copier la nouvelle clé publique (`cat ~/.ssh/id_rsa.pub | pbcopy`)
    1. Dans Bitbucket, allez ajouter la clé dans le repo du projet
    1. Retourner dans votre terminal et vérifier la connection avec : `ssh -T git@bitbucket.org`  

1. Base de données :
    1. Créer une nouvelle base de données. _Ne pas oublier de mettre les accès dans le document!_
    2. Aller ensuite y importer la vôtre avec le nom de domaine remplacé pour la production.  

        Avec wp-cli : `wp search-replace --url=<current-domain> <current-domain> <new-domain> --all-tables`  

1. Aller modifier les infos de serveur dans `config/hosts.php`.

1. Exécuter la commande de déploiement :  

    `npm run deploy prod`

1. Sur le serveur, aller porter dans le dossier `/shared` un nouveau `.env` avec les informations relatives à l'environnement de production

1. Au même endroit sur le serveur, aller porter les uploads dans le dossier `/shared/uploads`
