#!/bin/bash
#
# @author Alexandre Belley
# @date 27-08-2019
# @function Script d'installation de projet Bedrock

bold=`tput bold`
underline=`tput smul`
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

echo ""
echo "${green}Configuration d'un projet Bedrock${reset}"
echo "${green}*Les valeurs par défaut sont entre []${reset}"
echo ""
read -p "${bold}Est-ce un nouveau projet? (oui/non) ${reset}" newproject

# -- -- -- -- --  -- -- -- -- --  -- -- -- -- --  -- -- -- -- --
# SI nouveau projet
if [ $newproject == "oui" ]
then

  # Prompt pour chaque variables
  read -p "${bold}1/9 Titre du site: ${reset}" title
  read -p "${bold}2/9 Url de développement: ${reset}" url
  read -p "${bold}3/9 Url SSH du repo git: ${reset}" repo
  read -p "${bold}4/9 Nom de la base de données: ${reset}" dbname
  read -p "${bold}5/9 Utilisateur de la base de données [root]: ${reset}" dbuser
  dbuser=${dbuser:-root}
  read -p "${bold}6/9 Mot de passe de la base de données []: ${reset}" dbpass
  read -p "${bold}7/9 Nom d'utilisateur de l'admin [beet]: ${reset}" user
  user=${user:-beet}
  read -p "${bold}8/9 Mot de passe de l'admin [généré]: ${reset}" pass
  read -p "${bold}9/9 Courriel de l'admin [wordpress@boitebeet.com]: ${reset}" email
  email=${email:-wordpress@boitebeet.com}
  echo ""

  # Réinitialisation du repo git
  sudo rm -R .git/
  rm -R CHANGELOG.md
  git init
  git remote add origin $repo

  # Composer
  composer update

  # Fichier .env
  echo "${green}Création du fichier .env${reset}"
  sed -e "s|<url>|${url}|" -e "s|<dbname>|${dbname}|" -e "s|<dbuser>|${dbuser}|" -e "s|<dbpass>|${dbpass}|" .env.example > .env
  wp dotenv salts regenerate

  # Url du repo dans le fichier de déploiement
  sed "s|<repo>|${repo}|g" deploy.php > deploy-tmp.php
  mv deploy-tmp.php deploy.php

  # Installation de Wordpress
  echo "${green}Installation de Wordpress${reset}"
  wp core install --url=$url --title="${title}" --admin_user=$user --admin_password=$pass --admin_email=$email --skip-email

  # Activation du thème
  echo "${green}Activation du thème Beet${reset}"
  wp theme activate beet

  # Activation des plugins
  echo "${green}Activation de tous les plugins${reset}"
  wp plugin activate --all

  # Setup des branches
  echo "${green}Mise en place des branches git${reset}"
  git add .
  git commit -m "Initial commit"
  git push -u origin master

  git checkout -b staging
  git push -u origin staging

  git checkout -b develop
  git push -u origin develop

  # Message de succès
  echo ""
  echo "${green}${bold}Projet créé avec succès!${reset}"
  echo "${red}Important! ${green}N'oubliez pas de copier le mot de passe plus haut s'il a été généré, car il ne sera plus disponible${reset}"
  echo "${green}Votre site est accessible au ${underline}http://${url}${reset}"
  echo ""

# -- -- -- -- --  -- -- -- -- --  -- -- -- -- --  -- -- -- -- --
# SI projet existant
else

  # Prompt pour chaque variables
  read -p "${bold}1/4 Url de développement: ${reset}" url
  read -p "${bold}2/4 Nom de la base de données: ${reset}" dbname
  read -p "${bold}3/4 Utilisateur de la base de données [root]: ${reset}" dbuser
  dbuser=${dbuser:-root}
  read -p "${bold}4/4 Mot de passe de la base de données []: ${reset}" dbpass
  echo ""

  # Composer
  composer install

  # Fichier .env
  echo "${green}Création du fichier .env${reset}"
  sed -e "s|<url>|${url}|" -e "s|<dbname>|${dbname}|" -e "s|<dbuser>|${dbuser}|" -e "s|<dbpass>|${dbpass}|" .env.example > .env
  wp dotenv salts regenerate

  # Message de succès
  echo "${reset}"
  echo "${green}${bold}Projet installé avec succès!${reset}"
  echo "${green}Votre site est accessible au ${underline}http://${url}${reset}"
  echo ""

fi
