<?php

/**
 * @author Nicolas Milot
 * @date 2019-07-09
 * @function Listes des hosts et des configs pour le déploiement
 *
 * @use Deployer\host
 *
 */

use function Deployer\host;

/**
 * @env Développement
 * @branch Staging
 */
host('devbeet.com')
  ->user('devbeet')
  ->port(22)
  ->stage('staging')
  ->set('deploy_path', '~/public_html/bedrock')
  ->set('branch', 'staging');

/**
 * @env Production
 * @branch Master
 */
host('0.0.0.0')
  ->user('deploy')
  ->port(22)
  ->stage('production')
  ->set('deploy_path', '/var/www/bedrock')
  ->set('branch', 'master');
