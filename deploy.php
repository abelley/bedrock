<?php

/**
 * @author Nicolas Milot
 * @date 2019-07-09
 * @function Script de déploiement
 *
 * @use Deployer\cd
 * @use Deployer\set
 * @use Deployer\task
 * @use Deployer\run
 *
 * @require Common (Deployer's recipe)
 * @require Hosts (Homemade)
 *
 * @example php vendor/bin/dep deploy <stage>
 * @example php vendor/bin/dep rollback <stage>
 */

use function Deployer\cd;
use function Deployer\set;
use function Deployer\task;
use function Deployer\run;

require_once __DIR__ . '/vendor/deployer/deployer/recipe/common.php';
require_once __DIR__ . '/config/hosts.php';

/**
 * Défini le repo Bitbucket du projet
 * Utilise ssh
 */
set('repository', '<repo>');

/**
 * Défini les droits d'écriture et l'utilisateur http
 */
set('http_user', 'deploy');
set('writable_use_sudo', false);

/**
 * Défini les dossiers et les fichiers partagés entre les releases
 * Défini les dossiers et les fichiers libres d'écriture
 */

// Bedrock's shared dirs
set('shared_dirs', [
  'uploads'
]);

// Bedrock's shared files
set('shared_files', [
  '.env',
]);

// Bedrock's writable dirs
set('writable_dirs', [
]);

/**
 * Programmation des tâches de déploiement
 */

// Installation des modules composer
task('deploy:composer', function () {
  cd('{{release_path}}');
  run('composer install');
});

// Créé un lien symbolique vers le dossier uploads
task('deploy:uploads', function () {
  cd('{{release_path}}/web/app');
  run('rm -rf uploads');
  run('ln -s ../../../../shared/uploads uploads');
});

/**
 * Lance les tâches de déploiement
 */
task('deploy', [
  'deploy:info',
  'deploy:prepare',
  'deploy:lock',
  'deploy:release',
  'deploy:update_code',
  'deploy:composer',
  'deploy:uploads',
  'deploy:shared',
  'deploy:writable',
  'deploy:symlink',
  'deploy:unlock',
  'cleanup',
  'success'
]);
