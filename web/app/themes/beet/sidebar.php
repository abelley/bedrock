<aside class="sidebar">

  <?php get_search_form(); ?>

  <div class="title-sub"><?php _e('Catégories', 'beet'); ?></div>
  <ul>
    <?php wp_list_categories( array( 'title_li' => '' ) ); ?>
  </ul>

  <div class="title-sub"><?php _e('Archives', 'beet'); ?></div>
  <ul>
    <?php wp_get_archives( array( 'type' => 'monthly', 'limit' => 12 ) ); ?>
  </ul>

</aside>
