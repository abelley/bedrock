wp.domReady( () => {

  // Test styles (acf)
  wp.blocks.registerBlockStyle( 'acf/test', {
		name: 'default',
		label: 'Gris (Défaut)',
		isDefault: true
	} );

  wp.blocks.registerBlockStyle( 'acf/test', {
		name: 'blue',
		label: 'Bleu'
	} );

} );
