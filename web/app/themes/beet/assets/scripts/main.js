import Share from 'modules/social-shares';

// When everything loaded
window.addEventListener('load', () => {

  document.documentElement.classList.add('is-dom-loaded');

  // Shares
  const ShareLinks = document.querySelectorAll('.js-share');
  [...ShareLinks].forEach(Link => new Share(Link) );

});
