/*-
 * Social shares
 *
 * @selector .js-share
 * @required [href]
-*/

class Share {
  constructor(shareButton) {

    this.clickHandler = this.click.bind(this);
    shareButton.addEventListener('click', (event) => {
      event.preventDefault();
      this.clickHandler(shareButton.href);
    });
  }

  click(Link) {
    window.open(Link, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
  }
}
export default Share;
