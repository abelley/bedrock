function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

    return arr2;
  }
}

function _iterableToArray(iter) {
  if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance");
}

/*-
 * Social shares
 *
 * @selector .js-share
 * @required [href]
-*/
var Share =
/*#__PURE__*/
function () {
  function Share(shareButton) {
    var _this = this;

    _classCallCheck(this, Share);

    this.clickHandler = this.click.bind(this);
    shareButton.addEventListener('click', function (event) {
      event.preventDefault();

      _this.clickHandler(shareButton.href);
    });
  }

  _createClass(Share, [{
    key: "click",
    value: function click(Link) {
      window.open(Link, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
    }
  }]);

  return Share;
}();

window.addEventListener('load', function () {
  document.documentElement.classList.add('is-dom-loaded'); // Shares

  var ShareLinks = document.querySelectorAll('.js-share');

  _toConsumableArray(ShareLinks).forEach(function (Link) {
    return new Share(Link);
  });
});

//# sourceMappingURL=main.js.map
