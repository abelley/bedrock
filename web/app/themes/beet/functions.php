<?php
// ============================================
// FUNCTIONS.php
// where it all begins
// ============================================

require_once('includes/init.php');
require_once('includes/miscellaneous.php');
require_once('includes/enqueues.php');
require_once('includes/gutenberg.php');
require_once('includes/tiny-mce.php');

// PLUGINS
require_once('includes/acf.php');
require_once('includes/gravity-forms.php');

// CUSTOM POST TYPES
require_once('includes/custom-post-types/test.php');
