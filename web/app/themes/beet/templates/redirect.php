<?php
/*
Template Name: Redirection
*/

// Put this template on a parent page and it will redirect to his first children
if( have_posts() ):
  while( have_posts() ): the_post();

    $children = get_pages("child_of=".$post->ID."&sort_column=menu_order");
    $firstChild = $children[0];
    wp_redirect( get_permalink($firstChild->ID) );

  endwhile;
endif;
