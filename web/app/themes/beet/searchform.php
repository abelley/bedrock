<?php
$searchQuery = ( isset($_GET['s']) ) ? $_GET['s'] : '';
?>
<form class="search" method="get" action="<?= home_url() ?>" role="search">

	<input type="search" value="<?=$searchQuery?>" name="s" placeholder="<?php _e('Recherche', 'beet'); ?>" minlength="3" required>
	<input type="submit" class="button" value="<?php _e('Rechercher', 'beet'); ?>">

</form>
