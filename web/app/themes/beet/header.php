<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="HandheldFriendly" content="True">

  <?php
  // Google Tag Manager
  $gtm = get_field('gtm_code','options');
  if( $gtm ) : ?>
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','<?= $gtm ?>');</script>
  <?php endif; ?>

  <title><?php wp_title('-', true, 'right'); ?></title>

  <!-- Theming -->
  <?php
  $themeColor = "#C61262";
  $imagesPath = get_template_directory_uri().'/assets/images';
  $faviconPath = $imagesPath.'/favicon';
  ?>
  <meta name="theme-color" content="<?=$themeColor?>">
  <meta name="msapplication-TileColor" content="<?=$themeColor?>">
  <meta name="msapplication-TileImage" content="<?=$faviconPath?>/mstile-144x144.png">
  <meta name="msapplication-config" content="<?=$faviconPath?>/browserconfig.xml" />

  <link rel="apple-touch-icon" sizes="60x60" href="<?=$faviconPath?>/apple-touch-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="76x76" href="<?=$faviconPath?>/apple-touch-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="120x120" href="<?=$faviconPath?>/apple-touch-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="152x152" href="<?=$faviconPath?>/apple-touch-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="<?=$faviconPath?>/apple-touch-icon.png">

  <link rel="icon" type="image/png" href="<?=$faviconPath?>/favicon-16x16.png" sizes="16x16">
  <link rel="icon" type="image/png" href="<?=$faviconPath?>/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="<?=$faviconPath?>/favicon-96x96.png" sizes="96x96">
  <link rel="icon" type="image/png" href="<?=$faviconPath?>/android-chrome-192x192.png" sizes="192x192">

  <link rel="manifest" href="<?=$faviconPath?>/site.webmanifest">
  <link rel="mask-icon" href="<?=$faviconPath?>/safari-pinned-tab.svg" color="<?=$themeColor?>">
  <link rel="shortcut icon" href="<?=$faviconPath?>/favicon.ico">

  <!-- Scripts and more -->
  <?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

  <?php if( $gtm ) : ?>
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?= $gtm ?>"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <?php endif; ?>

  <header class="header wrapper wrapper--full">

    <a href="<?= esc_url( home_url('/') ) ?>" class="header__logo">
      <img src="<?=$imagesPath?>/logo.svg" alt="<?php bloginfo('name'); ?>">
    </a>

    <nav class="header__nav">
      <?php wp_nav_menu( array(
        'theme_location' => 'main',
        'container' => ''
      )); ?>
    </nav>

  </header>

  <main>
