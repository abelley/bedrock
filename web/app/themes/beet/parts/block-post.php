<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <header>
    <h2 class="title-sub"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <?php printf( __('Le %s par %s', 'beet'), get_the_date(), get_the_author() ); ?>
  </header>

  <?php the_excerpt(); ?>

  <footer>
    <a href="<?php the_permalink(); ?>"><?php _e('Voir plus','beet') ?></a>
  </footer>

</article>
