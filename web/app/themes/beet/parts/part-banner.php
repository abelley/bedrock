<div class="banner">

  <?php if( has_post_thumbnail() ): ?>

    <?php
    $postID = get_the_ID();
    $banner_alt = get_post_meta($postID, '_wp_attachment_image_alt', TRUE);
    $banner_mobile_portrait = get_the_post_thumbnail_url($postID, 'banner-mobile-portrait');
    $banner_mobile_landscape = get_the_post_thumbnail_url($postID, 'banner-mobile-landscape');
    $banner_desktop_x = get_the_post_thumbnail_url($postID, 'banner-desktop');
    $banner_desktop_xx = get_the_post_thumbnail_url($postID, 'banner-desktop-2x');
    ?>

    <picture class="banner__image">
      <source media="(max-width: 740px) and (orientation: portrait)" srcset="<?=$banner_mobile_portrait?>">
      <source media="(max-width: 740px) and (orientation: landscape)" srcset="<?=$banner_mobile_landscape?>">
      <source media="(min-width: 740px)" srcset="<?=$banner_desktop_x?> 1x, <?=$banner_desktop_xx?> 2x">
      <img src="<?=$banner_desktop_x?>" alt="<?=$banner_alt?>">
    </picture>

  <?php endif; ?>

  <h1 class="banner__title title-main"><?php the_title(); ?></h1>

</div>
