<?php
$socials = array(
  'facebook',
  'twitter',
  'instagram',
  'linkedin',
  'youtube',
  'vimeo',
  'pinterest'
);
?>
<ul class="socials">
  <?php foreach ($socials as $social):
    $socialLink = get_field($social, 'options');
    if ($socialLink): ?>
      <li>
        <a href="<?=$socialLink?>" target="_blank" rel="noreferrer">
          <?php icon($social); ?>
          <span class="visuallyhidden"><?=$social?></span>
        </a>
      </li>
    <?php endif; ?>
  <?php endforeach; ?>
</ul>
