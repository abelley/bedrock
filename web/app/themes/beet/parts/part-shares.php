<?php
$permalink = get_permalink();
$onClick = "javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;";
?>
<div class="shares">

  <div class="shares__title"><?php _e('Partager', 'beet'); ?></div>

  <ul class="shares__links">
    <li>
      <a href="https://www.facebook.com/sharer/sharer.php?u=<?=$permalink?>" class="js-share">
        <?php icon('facebook'); ?>
        <span class="visuallyhidden"><?php _e('Partager sur Facebook', 'beet'); ?></span>
      </a>
    </li>
    <li>
      <a href="https://www.twitter.com/share?url=<?=$permalink?>" class="js-share">
        <?php icon('twitter'); ?>
        <span class="visuallyhidden"><?php _e('Partager sur Twitter', 'beet'); ?></span>
      </a>
    </li>
  </ul>

</div>
