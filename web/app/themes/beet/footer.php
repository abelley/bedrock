  </main>

  <footer class="footer wrapper wrapper--full">

    <p>© <?= date('Y') ?> <?php bloginfo('name'); ?></p>
    <?php get_template_part('parts/part', 'socials'); ?>
    <p><?php _e('Par','beet'); ?> <a href="http://boitebeet.com/" target="_blank" rel="noreferrer noopener">Beet</a></p>

  </footer>

  <?php wp_footer(); ?>

</body>
</html>
