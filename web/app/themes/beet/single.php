<?php get_header(); ?>

<?php if (have_posts()): ?>
  <?php while (have_posts()): the_post(); ?>

    <div class="wrapper">

      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <header>
          <h1 class="title-main"><?php the_title(); ?></h1>
          <?php printf( __('Le %s par %s', 'beet'), get_the_date(), get_the_author() ); ?>
        </header>

        <div class="typography">
          <?php
          if( has_post_thumbnail() )
            the_post_thumbnail();
          ?>
          <?php the_content(); ?>
        </div>

        <footer>
          <?php get_template_part('parts/part', 'shares'); ?>
          <nav>
            <?php previous_post_link('%link', __('Article précédent', 'beet') ); ?>
            <?php next_post_link('%link', __('Article suivant', 'beet') ); ?>
          </nav>
        </footer>

      </article>

      <?php get_sidebar(); ?>

    </div>

  <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
