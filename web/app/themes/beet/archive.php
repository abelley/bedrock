<?php
global $wp_query;
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
?>

<?php get_header(); ?>

<div class="wrapper">

  <h1 class="title-main">
    <?php
    if ( is_category() ):
      single_cat_title();
    elseif ( is_post_type_archive() ):
      post_type_archive_title();
    else:
      _e('Archives', 'beet');
    endif;
    ?>
  </h1>

  <?php if (have_posts()): ?>
    <?php while (have_posts()): the_post(); ?>

      <?php get_template_part('parts/block', 'post'); ?>

    <?php endwhile; ?>
  <?php else: ?>

    <?php _e('Aucun article', 'beet'); ?>

  <?php endif; ?>

  <div class="pagination">
    <?php
    echo paginate_links(array(
      'format' => 'page/%#%',
      'current' => $paged,
      'total' => $wp_query->max_num_pages,
      'type' => 'list',
      'prev_text' => __('Précédent', 'beet'),
      'next_text' => __('Suivant', 'beet')
    ));
    ?>
  </div>

  <?php get_sidebar(); ?>

</div>

<?php get_footer(); ?>
