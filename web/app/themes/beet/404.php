<?php get_header(); ?>

<div class="typography wrapper">

  <h1 class="title-main"><?php _e('404 Page introuvable', 'beet') ?></h1>
  <p><?php _e('La page que vous recherchez a été déplacée ou supprimée.', 'beet') ?></p>
  <p><a href="<?= esc_url( home_url('/') ); ?>" class="link"><?php _e('Retour à l’accueil', 'beet') ?></a></p>

</div>

<?php get_footer(); ?>
