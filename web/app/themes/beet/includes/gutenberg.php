<?php
// ============================================
// FUNCTIONS - GUTENBERG
// ============================================

// ENQUEUES
function gutenberg_scripts() {

  $editorPath = get_stylesheet_directory_uri().'/assets/scripts/editor.js';
	wp_enqueue_script('beet-editor-scripts', $editorPath, array('wp-blocks', 'wp-dom'), NULL, true);
}
add_action('enqueue_block_editor_assets', 'gutenberg_scripts');

// SUPPORTS
function gutenberg_support() {
  add_theme_support('align-wide');
  add_theme_support('editor-styles');
  add_theme_support('disable-custom-font-sizes');
  add_theme_support('disable-custom-colors');
  add_editor_style('assets/dist/editor.css');
}
add_action('after_setup_theme', 'gutenberg_support');

function gutenberg_allowed_block_types($allowed_blocks, $post) {

  $allowed_blocks = array(
    // Common Blocks
    'core/paragraph',
    'core/image',
		'core/heading',
		'core/list',
		'core/quote',
		'core/audio',
		'core/video',

		// Formatting
    'core/code',
    'core/freeform', //check
		'core/html',
    'core/table',

    // Layout Elements
		'core/column',
		'core/columns',
		'core/media-text',
		'core/separator',

    // Widgets
		// 'core/group',
		'core/shortcode',
		'core/search',

    // Embeds
    'gravityforms/form',

    // ACF custom blocks
		'acf/test',
	);

	return $allowed_blocks;
}
add_filter('allowed_block_types', 'gutenberg_allowed_block_types', 10, 2);
