<?php
// ============================================
// FUNCTIONS - General
// ============================================

global $sitepress;

// Remove items from admin menu
function remove_menus() {
  global $current_user;
  $user_id = get_current_user_id();
  remove_menu_page('edit-comments.php');

  if($user_id != '1') {
    remove_menu_page('themes.php');
    remove_menu_page('users.php');
    remove_menu_page('tools.php');
    remove_menu_page('options-general.php');
    remove_menu_page('plugins.php');
    remove_submenu_page('index.php', 'update-core.php');

    remove_menu_page('edit.php?post_type=acf-field-group');
    remove_menu_page('theseoframework-settings');
    remove_menu_page('wp-mail-smtp');
    remove_menu_page('sitepress-multilingual-cms/menu/languages.php');

    add_menu_page('Menu', 'Menu', 'edit_posts', 'nav-menus.php', '', 'dashicons-menu', 20);
    add_menu_page('Profil', 'Profil', 'edit_posts', 'profile.php', '', 'dashicons-admin-users', 21);
  }
}
add_action('admin_init', 'remove_menus');

// Hide some dashboard widgets
function remove_dashboard_meta() {
  remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
  remove_meta_box('dashboard_primary', 'dashboard', 'normal');
  remove_meta_box('dashboard_activity', 'dashboard', 'normal');
}
add_action('admin_init', 'remove_dashboard_meta');

// Disable emojis
function disable_emojicons_tinymce($plugins) {
  if (is_array($plugins)) {
    return array_diff($plugins, array('wpemoji'));
  } else {
    return array();
  }
}

function disable_wp_emojicons() {
  remove_action('admin_print_styles', 'print_emoji_styles');
  remove_action('wp_head', 'print_emoji_detection_script', 7);
  remove_action('admin_print_scripts', 'print_emoji_detection_script');
  remove_action('wp_print_styles', 'print_emoji_styles');
  remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
  remove_filter('the_content_feed', 'wp_staticize_emoji');
  remove_filter('comment_text_rss', 'wp_staticize_emoji');
  add_filter('tiny_mce_plugins', 'disable_emojicons_tinymce');
}
add_action('init', 'disable_wp_emojicons');

// Remove new content from admin bar and remove it completely on front end
function remove_wp_nodes() {
  global $wp_admin_bar;
  $wp_admin_bar->remove_node('new-content');
  $wp_admin_bar->remove_node('comments');
}
add_action('admin_bar_menu', 'remove_wp_nodes', 999);

add_filter('show_admin_bar', '__return_false');

// Remove comment support so we don't get spammed
function remove_comment_support() {
  remove_post_type_support('post', 'comments');
  remove_post_type_support('page', 'comments');
  remove_post_type_support('attachment', 'comments');
}
add_action('init', 'remove_comment_support');



// ============================================
// FUNCTIONS - Images
// ============================================

// Remove default image sizes and caption width
function remove_default_image_sizes($sizes) {
  unset($sizes['medium']);
  unset($sizes['medium_large']);
  unset($sizes['large']);

  return $sizes;
}
add_filter('intermediate_image_sizes_advanced', 'remove_default_image_sizes');

function remove_caption_extra_width($width) {
  return $width - 10;
}
add_filter('img_caption_shortcode_width', 'remove_caption_extra_width');

// Clean up files' name when uploading
function wpc_sanitize_french_chars($filename) {

	$filename = mb_convert_encoding($filename, "UTF-8");
	$char_not_clean = array('/À/','/Á/','/Â/','/Ã/','/Ä/','/Å/','/Ç/','/È/','/É/','/Ê/','/Ë/','/Ì/','/Í/','/Î/','/Ï/','/Ò/','/Ó/','/Ô/','/Õ/','/Ö/','/Ù/','/Ú/','/Û/','/Ü/','/Ý/','/à/','/á/','/â/','/ã/','/ä/','/å/','/ç/','/è/','/é/','/ê/','/ë/','/ì/','/í/','/î/','/ï/','/ð/','/ò/','/ó/','/ô/','/õ/','/ö/','/ù/','/ú/','/û/','/ü/','/ý/','/ÿ/', '/©/');
	$clean = array('a','a','a','a','a','a','c','e','e','e','e','i','i','i','i','o','o','o','o','o','u','u','u','u','y','a','a','a','a','a','a','c','e','e','e','e','i','i','i','i','o','o','o','o','o','o','u','u','u','u','y','y','copy');
	$friendly_filename = preg_replace($char_not_clean, $clean, $filename);

	$friendly_filename = utf8_decode($friendly_filename);
	$friendly_filename = preg_replace('/\?/', '', $friendly_filename);

	$friendly_filename = strtolower($friendly_filename);

	return $friendly_filename;
}
add_filter('sanitize_file_name', 'wpc_sanitize_french_chars', 10);



// ============================================
// FUNCTIONS - Remove unnecessary stuff in head
// ============================================

remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rest_output_link_wp_head');
remove_action('wp_head', 'wp_oembed_add_discovery_links');
