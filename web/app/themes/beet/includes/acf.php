<?php
// ============================================
// FUNCTIONS - Advanced Custom Fields (ACF)
// ============================================

// ADD OPTION PAGE
function register_acf_options_pages() {

  if( !function_exists('acf_add_options_page') )
    return;

  $option_page = acf_add_options_page(array(
    'page_title' => __('Configurations', 'beet'),
    'menu_title' => __('Configurations', 'beet'),
    'menu_slug' => 'configurations'
  ));
}
add_action('acf/init', 'register_acf_options_pages');

// DECLARE GOOGLE API KEY
function init_google_api_key() {
	acf_update_setting('google_api_key', '<google-api-key-here>');
}
add_action('acf/init', 'init_google_api_key');

// LOCAL JSON SAVE AND LOAD POINTS
function acf_json_save_point($path) {
  $path = get_stylesheet_directory().'/includes/acf-json';
  return $path;
}
add_filter('acf/settings/save_json', 'acf_json_save_point');

function acf_json_load_point($paths) {
  unset($paths[0]);
  $paths[] = get_stylesheet_directory().'/includes/acf-json';
  return $paths;
}
add_filter('acf/settings/load_json', 'acf_json_load_point');

// CUSTOM GUTENBERG BLOCKS
function acf_custom_blocks() {
	if( function_exists('acf_register_block') ) {

    // Test
		acf_register_block(array(
			'name' => 'test',
			'title' => __('Test'),
			'render_callback' => 'acf_blocks_render_callback',
			'category' => 'common',
			'icon' => 'tag',
      'supports' => array(
        'align' => true
      )
		));

	}
}
add_action('acf/init', 'acf_custom_blocks');

// ACF blocks callback (dynamic file name)
function acf_blocks_render_callback($block) {

	$slug = str_replace('acf/', '', $block['name']);

	if( file_exists( get_theme_file_path("/blocks/{$slug}.php") ) ) {
		include( get_theme_file_path("/blocks/{$slug}.php") );
	}
}
