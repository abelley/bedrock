<?php
// ============================================
// FUNCTIONS - TINY MCE
// ============================================

// CUSTOM BUTTONS LIST
function wysiwyg_buttons_first_row($buttons) {

  $buttons = array(
    'bold',
    'italic',
    'underline',
    'strikethrough',
    'superscript',
    'subscript',
    'blockquote',
    'hr',
    'bullist',
    'numlist',
    'alignleft',
    'aligncenter',
    'alignright',
    'link',
    'unlink',
    'wp_adv'
  );

  return $buttons;
}
add_filter('mce_buttons', 'wysiwyg_buttons_first_row');

function wysiwyg_buttons_second_row($buttons) {

  $buttons = array(
    'formatselect',
    'styleselect',
    'pastetext',
    'removeformat',
    'charmap',
    'outdent',
    'indent',
    'undo',
    'redo'
  );

  return $buttons;
}
add_filter('mce_buttons_2', 'wysiwyg_buttons_second_row');

// CUSTOM FORMATS
function wysiwyg_custom_formats($init_array) {

  $style_formats = array(
    array(
      'title' => 'Titres',
      'items' => array(
        array(
          'title' => 'Titre principal',
          'selector' => 'h1, h2, h3, h4, h5, h6',
          'classes' => 'title-main'
        ),
        array(
          'title' => 'Sous-titre',
          'selector' => 'h1, h2, h3, h4, h5, h6',
          'classes' => 'title-sub'
        )
      )
    ),
    array(
      'title' => 'Boutons',
      'items' => array(
        array(
          'title' => 'Bouton',
          'selector' => 'a, button',
          'classes' => 'button'
        ),
        array(
          'title' => 'Lien',
          'selector' => 'a, button',
          'classes' => 'link'
        )
      )
    )
  );

	$init_array['style_formats'] = json_encode( $style_formats );
  $init_array['block_formats'] = 'Paragraph=p;Titre 2=h2;Titre 3=h3;Titre 4=h4';

  return $init_array;
}
add_filter('tiny_mce_before_init', 'wysiwyg_custom_formats');
