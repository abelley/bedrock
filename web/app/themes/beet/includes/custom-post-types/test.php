<?php
// ============================================
// FUNCTIONS - CUSTOM POST TYPES - TEST
// ============================================

function create_cpt_test() {

	register_post_type('test', array(
		'labels' => array(
			'name' => 'Tests',
			'singular_name' => 'Test',
			'add_new' => 'Ajouter',
			'add_new_item' => 'Ajouter un nouveau test',
			'edit' => 'Modifier',
			'edit_item' => 'Modifier le test',
			'new_item' => 'Ajouter un test',
			'view' => 'Voir',
			'view_item' => 'Voir le test',
			'search_items' => 'Rechercher dans les tests',
			'not_found' => 'Aucun résultat',
			'not_found_in_trash' => 'Aucun résultat dans la corbeille'
		),
		'has_archive' => true,
		'public' => true,
		'show_in_rest' => true, // Gutenberg activation. Remove to use classic editor.
		'menu_position' => 21,
		'menu_icon' => 'dashicons-tag',
		'supports' => array(
			'title',
			'editor',
			'thumbnail'
		),
		'rewrite' => array(
			'slug' => 'test'
		)
	));

	register_taxonomy('taxonomy-test', 'test',
		array(
			'hierarchical' => true,
			'show_admin_column' => true,
			'label' => __('Taxonomy Test')
		)
	);

}
add_action('init', 'create_cpt_test');
