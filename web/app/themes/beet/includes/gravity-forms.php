<?php
// ============================================
// FUNCTIONS - GRAVITY FORMS
// ============================================

// REMOVE ALL ENQUEUED STYLES
function gform_dequeue_styles() {
	wp_deregister_style("gforms_reset_css");
	wp_deregister_style("gforms_formsmain_css");
	wp_deregister_style("gforms_ready_class_css");
	wp_deregister_style("gforms_browsers_css");
}
add_action('gform_enqueue_scripts', 'gform_dequeue_styles');

// CHANGE DEFAULT LOADER
function gform_custom_spinner($src) {
	$spinner_src = get_stylesheet_directory_uri().'/assets/images/loader.svg';
	return $spinner_src;
}
add_filter('gform_ajax_spinner_url', 'gform_custom_spinner');

// REMOVE SCROLL TO TOP OF FORM ON SUBMIT
add_filter('gform_confirmation_anchor', '__return_false');
