<?php
// ============================================
// FUNCTIONS - MISCELLANEOUS
// ============================================

// CUSTOM PRINT VAR FUNCTION
//
//    prt($foo);
//    prt($bar, false);
//
function prt($data, $print = true) {
  echo '<pre>';
  if ($print)
    print_r($data);
  else
    var_dump($data);
  echo '</pre>';
}


// APPEND SVG ICON
// For complete list of icons, check in folder /assets/images/icons
//
//    icon('<icon-name>', false);
//
function icon($icon, $echo = true) {
  $assetsPath = get_template_directory_uri();
  $iconHTML = '
  <svg class="icon" role="img">
    <use xlink:href="'.$assetsPath.'/assets/images/sprite.svg#'.$icon.'"></use>
  </svg>';

  if ($echo)
    echo $iconHTML;
  else
    return $iconHTML;
}


// ADD MENUS
add_theme_support('menus');

function register_menu() {
	register_nav_menus(array(
		'main' => 'Menu principal'
	));
}
add_action('init', 'register_menu');


// IMAGE SIZES
add_theme_support('post-thumbnails');

add_image_size('banner-mobile-portrait', 840, 650, true);
add_image_size('banner-mobile-landscape', 1400, 600, true);
add_image_size('banner-desktop', 1920, 600, true);
add_image_size('banner-desktop-2x', 3840, 1200, true);

function custom_image_sizes_names($sizes) {
  return array_merge($sizes, array(
    'banner-mobile-portrait' => __('Mobile Portrait', 'beet'),
    'banner-mobile-landscape' => __('Mobile Paysage', 'beet'),
    'banner-desktop' => __('Bureau', 'beet'),
    'banner-desktop-2x' => __('Bureau (retina)', 'beet')
  ));
}
add_filter('image_size_names_choose', 'custom_image_sizes_names');


// THE SEO FRAMEWORK
// Remove the plugin credits in head
add_filter('the_seo_framework_indicator', '__return_false');
