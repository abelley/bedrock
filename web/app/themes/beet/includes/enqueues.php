<?php
// ============================================
// FUNCTIONS - ENQUEUES STYLES AND SCRIPTS
// ============================================

function enqueues() {

  $template = get_template_directory_uri();
  $mainCSS = ( $_ENV['WP_ENV'] == 'development' ) ? 'main.css' : 'main.min.css';
  $mainJS = ( $_ENV['WP_ENV'] == 'development' ) ? 'main.js' : 'main.min.js';
  $google_api_key = acf_get_setting('google_api_key');

  wp_deregister_script('jquery');
  wp_deregister_script('wp-embed');
  wp_deregister_style('wp-block-library');

  wp_enqueue_style('fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:400,700&display=swap', array(), false, 'all');
  wp_enqueue_style('main-css', "$template/assets/dist/$mainCSS", false, null);

  wp_enqueue_script('modernizr', $template.'/assets/scripts/vendor/modernizr.custom.js', false, null);
  wp_enqueue_script('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js', false, '2.2.4');
  wp_enqueue_script('map', 'https://maps.googleapis.com/maps/api/js?key='.$google_api_key, false, null, true);
  wp_enqueue_script('main-js', "$template/assets/dist/$mainJS", array(), null, true);

  wp_localize_script('main-js', 'theme_paths', array(
    'ajax' => admin_url('admin-ajax.php'),
    'assets' => "$template/assets/"
  ));

}
add_action('wp_enqueue_scripts', 'enqueues');
