<?php get_header(); ?>

<div class="wrapper">

  <?php get_template_part('parts/part', 'banner'); ?>

  <div class="typography">
    <p><button class="button">Un bouton</button></p>
    <p><button class="link">Un lien</button></p>
  </div>

</div>

<?php get_footer(); ?>
