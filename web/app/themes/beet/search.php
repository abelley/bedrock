<?php
global $wp_query;
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$postCount = $wp_query->found_posts;
?>

<?php get_header(); ?>

<div class="wrapper">

  <?php get_search_form(); ?>

  <h1 class="title-main">
    <?php
    if ($postCount == 0):
      printf( __('Aucun résultat de recherche', 'beet'), $postCount);
    elseif ($postCount == 1):
      printf( __('<strong>%s</strong> résultat de recherche', 'beet'), $postCount);
    else:
      printf( __('<strong>%s</strong> résultats de recherche', 'beet'), $postCount);
    endif;
    ?>
  </h1>

  <?php if (have_posts()): ?>
    <?php while (have_posts()): the_post(); ?>

      <?php get_template_part('parts/block', 'post'); ?>

    <?php endwhile; ?>
  <?php else: ?>

    <p class="no-result">
      <?php _e('Votre recherche n’a donné aucun résultat. <br>Vérifiez l’orthographe des termes utilisés ou essayer avec d’autres mots clés.', 'beet'); ?>
    </p>

  <?php endif; ?>

  <div class="pagination">
    <?php
    echo paginate_links(array(
      'format' => 'page/%#%',
      'current' => $paged,
      'total' => $wp_query->max_num_pages,
      'type' => 'list',
      'prev_text' => __('Précédent', 'beet'),
      'next_text' => __('Suivant', 'beet')
    ));
    ?>
  </div>

</div>

<?php get_footer(); ?>
