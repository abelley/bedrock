<?php
/**
 * Block Name: Test
 */

$test = get_field('test-field');

$align = ( $block['align'] ) ? 'align'.$block['align'] : '';
$classes = ( isset($block['className']) ) ? $block['className'] : '';
?>
<div class="wp-block-test <?=$align?> <?=$classes?>">

  <h2 class="title-main">
    <?= ($test) ? $test : '<span class="field-empty">'.__('Champ test', 'beet').'</span>' ?>
  </h2>

</div>
